package com.moneyveo.ukr_uat.moneyveo.api;

import com.moneyveo.ukr_uat.moneyveo.models.AuthProviderModel;
import com.moneyveo.ukr_uat.moneyveo.models.BodyModel;
import com.moneyveo.ukr_uat.moneyveo.models.ResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by aleksandr on 4/12/17.
 */

public interface API {
    @POST("/Veo.Web.Api.PreProd/api/Auth/")
    Call<ResponseModel> login(@Body BodyModel user);

    @GET("/Veo.Web.Api.PreProd/api/AuthProvider")
    Call<AuthProviderModel> authProvider(
            @Query("ProviderToken") String providerToken
    );
}
