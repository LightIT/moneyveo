package com.moneyveo.ukr_uat.moneyveo.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.moneyveo.ukr_uat.moneyveo.models.AuthProviderModel;
import com.moneyveo.ukr_uat.moneyveo.models.BodyModel;
import com.moneyveo.ukr_uat.moneyveo.R;
import com.moneyveo.ukr_uat.moneyveo.Utils;
import com.moneyveo.ukr_uat.moneyveo.api.API;
import com.moneyveo.ukr_uat.moneyveo.api.ApiEvent;
import com.moneyveo.ukr_uat.moneyveo.models.ResponseModel;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.HTTP;

import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTH;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTHORIZATION_ACCOUNT_IS_BLOCKED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTHORIZATION_AUTHORIZATION_REQUIRED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTHORIZATION_FOUND_MORE_THAN_ONE_USER_WITH_THE_PHONE_NUMBER;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTHORIZATION_INCORRECT_PASSWORD;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTHORIZATION_PARAMETERS_VALIDATION_FAILED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTHORIZATION_TOKEN_NOT_ACTIVE;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTHORIZATION_TOKEN_NOT_FOUND;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTHORIZATION_USER_NOT_FOUND;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTH_PROVIDER;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.COMMUNICATION_GENERAL_ERROR;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.COMMUNICATION_HTTPS_IS_REQUIERED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.EMAIL;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.GENERAL_VALIDATION_AUTH_HEADER_PARSE_ERROR;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.GENERAL_VALIDATION_MODEL_VALIDATION_ERROR;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.GENERAL_VALIDATION_PARAMETER_IS_REQUIRED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.GENERAL_VALIDATION_WORKFLOW_VALIDATION_ERROR;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.INVALID_PROVIDER_TOKEN;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.IS_INTERNET_ACCESS;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PASSWORD;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PROVIDER_AUTHORIZATION_AUTHORIZATION_REQUIERED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PROVIDER_AUTHORIZATION_AUTHORIZED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PROVIDER_AUTHORIZATION_PARAMETERS_VALIDATION_IS_FAILED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PROVIDER_AUTHORIZATION_PROVIDER_IS_BLOCKED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PROVIDER_AUTHORIZATION_TOKEN_NOT_FOUND;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PROVIDER_TOKEN;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.SUCCEEDED;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.TOKEN;

/**
 * Created by aleksandr on 4/12/17.
 */

public class DataLoaderService extends IntentService {


    private static final String HEADER_AUTH = "Authorization: VEO ";
    private static final String AUTHORIZED = "ProviderAuthorization: Authorized";
    private final static String TAG = "DataLoaderService";

    public DataLoaderService() {
        super(TAG);
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public DataLoaderService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (!isOnline()) {
            sendEvent(IS_INTERNET_ACCESS, true, null);
            return;
        }
        switch (intent.getAction()) {
            case ServiceConstants.USER_LOGIN:
                login(intent);
                break;
            case ServiceConstants.PROVIDER_TOKEN:
                authProvider(intent);
                break;
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    void login(Intent intent) {
        Log.d(TAG, "login");
        String email = intent.getStringExtra(EMAIL);
        String password = intent.getStringExtra(PASSWORD);
        API api = getApi(true);
        Call<ResponseModel> call = api.login(new BodyModel(email, password));
        call.enqueue(new ResponseCallback<ResponseModel>() {

            @Override
            public void onSuccess(Call<ResponseModel> call, Response<ResponseModel> response) {
                responseLogin(response);
            }

            @Override
            public void onError(Call<ResponseModel> call, Response<ResponseModel> response) {
                responseLogin(response);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e(TAG, "login failure " + t.toString());
                sendEvent(AUTH, false, getApplicationContext().getString(R.string.unknown_error));
            }
        });
    }

    void authProvider(Intent intent) {
        String providerToken = intent.getStringExtra(PROVIDER_TOKEN);

        API api = getApi(true);
        Call<AuthProviderModel> authProviderModelCall = api.authProvider(providerToken);
        authProviderModelCall.enqueue(new ResponseCallback<AuthProviderModel>() {
            @Override
            public void onSuccess(Call<AuthProviderModel> call, Response<AuthProviderModel> response) {
                Log.d(TAG, "authProvider response code " + response.code());
                Log.d(TAG, "authProvider response " + response.body());
                responseProviderToken(response);
            }

            @Override
            public void onError(Call<AuthProviderModel> call, Response<AuthProviderModel> response) {
                Log.e(TAG, "authProvider failure " + response.toString());
                responseProviderToken(response);
            }

            @Override
            public void onFailure(Call<AuthProviderModel> call, Throwable t) {
                Log.e(TAG, "authProvider failure " + t.toString());
                sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.unknown_error));
            }
        });
    }

    API getApi(boolean addAuthHeader) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (addAuthHeader)
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header(HEADER_AUTH + getPreparedProviderTokenForAddingToHeader(), getPreparedTokenForAddingToHeader());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.api_url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit.create(API.class);
    }

    String getPreparedTokenForAddingToHeader() {
        String token = Utils.getToken(getApplicationContext());
//        if (token.equals(""))
//            return "";
        return token;
    }

    String getPreparedProviderTokenForAddingToHeader() {
        String providerToken = Utils.getProviderToken(getApplicationContext());
        return providerToken;
    }

    void sendEvent(String eventType, boolean success, String errorMessage) {
        Log.d(TAG, "sendEvent event type " + eventType);
        Log.d(TAG, " success " + success);
        Log.d(TAG, " error " + errorMessage);
        ApiEvent apiEvent = new ApiEvent(eventType, success, errorMessage);
        EventBus.getDefault().post(apiEvent);
    }

    String proceedError(Response response) {
        String errorJsonString=null;
        try {
            errorJsonString = response.errorBody().string();
            Log.d(TAG, "response " + errorJsonString);
            String errors = ApiUtils.parseError(errorJsonString);
            Log.d(TAG, "errors " + errors);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return errorJsonString;
    }

    void responseProviderToken(Response<AuthProviderModel> response) {
        float code = 0f;
        if (response.code() == HttpURLConnection.HTTP_OK) {
            code = response.body().getReasons().get(0).getCode();
            if (response.body().getReasons().get(0).getCodeDescription().equals(AUTHORIZED))
                sendEvent(AUTH_PROVIDER, true, null);
        } else {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            ResponseModel responseModel = gson.fromJson(proceedError(response), ResponseModel.class);
            code = responseModel.getReasons().get(0).getCode();
        }

        if (code == PROVIDER_AUTHORIZATION_AUTHORIZATION_REQUIERED)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.provider_authorization_authorization_required));
        else if (code == PROVIDER_AUTHORIZATION_PROVIDER_IS_BLOCKED)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.provider_authorization_provider_is_blocked));
        else if (code == PROVIDER_AUTHORIZATION_TOKEN_NOT_FOUND)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.provider_authorization_token_not_found));
        else if (code == COMMUNICATION_GENERAL_ERROR)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.communication_general_error));
        else if (code == COMMUNICATION_HTTPS_IS_REQUIERED)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.communication_https_is_requiered));
        else if (code == GENERAL_VALIDATION_AUTH_HEADER_PARSE_ERROR)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.general_validation_auth_header_parse_error));
        else if (code == GENERAL_VALIDATION_MODEL_VALIDATION_ERROR)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.general_validation_model_validation_error));
        else if (code == GENERAL_VALIDATION_PARAMETER_IS_REQUIRED)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.general_validation_parameter_is_required));
        else if (code == GENERAL_VALIDATION_WORKFLOW_VALIDATION_ERROR)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.general_validation_workflow_validation_error));
        else if (code == PROVIDER_AUTHORIZATION_AUTHORIZATION_REQUIERED)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.provider_authorization_authorization_required));
        else if (code == PROVIDER_AUTHORIZATION_PARAMETERS_VALIDATION_IS_FAILED)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.provider_authorization_parameters_validation_failed));
        else if (code == PROVIDER_AUTHORIZATION_PROVIDER_IS_BLOCKED)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.provider_authorization_provider_is_blocked));
        else if (code == PROVIDER_AUTHORIZATION_TOKEN_NOT_FOUND)
            sendEvent(AUTH_PROVIDER, false, getApplicationContext().getString(R.string.provider_authorization_token_not_found));
    }

    void responseLogin(Response<ResponseModel> response) {
        float code = 0f;
        if (response.code() == HttpURLConnection.HTTP_OK) {
            Log.d(TAG, "login response code " + response.code());
            Log.d(TAG, "login response " + response.body());
            Log.d(TAG, "login token " + response.body().getToken());
            code = response.body().getReasons().get(0).getCode();
            if (response.body().getResultDescription().equals(SUCCEEDED)) {
                Utils.saveToken(getApplicationContext(), response.body().getToken());
                sendEvent(AUTH, true, null);
            }
        } else {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            ResponseModel responseModel = gson.fromJson(proceedError(response), ResponseModel.class);
            code = responseModel.getReasons().get(0).getCode();
            Log.e(TAG, "login failure " + response.toString());

        }
        if (code == COMMUNICATION_GENERAL_ERROR)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.communication_general_error));
        else if (code == COMMUNICATION_HTTPS_IS_REQUIERED)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.communication_https_is_requiered));
        else if (code == GENERAL_VALIDATION_AUTH_HEADER_PARSE_ERROR)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.general_validation_auth_header_parse_error));
        else if (code == GENERAL_VALIDATION_MODEL_VALIDATION_ERROR)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.general_validation_model_validation_error));
        else if (code == GENERAL_VALIDATION_PARAMETER_IS_REQUIRED)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.general_validation_parameter_is_required));
        else if (code == GENERAL_VALIDATION_WORKFLOW_VALIDATION_ERROR)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.general_validation_workflow_validation_error));
        else if (code == AUTHORIZATION_INCORRECT_PASSWORD)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.authorization_incorrect_password));
        else if (code == AUTHORIZATION_PARAMETERS_VALIDATION_FAILED)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.authorization_parameters_validation_failed));
        else if (code == AUTHORIZATION_USER_NOT_FOUND)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.authorization_user_not_found));
        else if (code == AUTHORIZATION_FOUND_MORE_THAN_ONE_USER_WITH_THE_PHONE_NUMBER)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.authorization_found_more_than_one_user_with_the_phone_number));
        else if (code == AUTHORIZATION_ACCOUNT_IS_BLOCKED)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.authorization_account_is_blocked));
        else if (code == AUTHORIZATION_TOKEN_NOT_FOUND)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.authorization_token_not_found));
        else if (code == AUTHORIZATION_TOKEN_NOT_ACTIVE)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.authorization_token_not_active));
        else if (code == AUTHORIZATION_AUTHORIZATION_REQUIRED)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.authorization_authorization_required));
        else if (code == INVALID_PROVIDER_TOKEN)
            sendEvent(AUTH, false, getApplicationContext().getString(R.string.invalid_provider_token));
    }
}
