package com.moneyveo.ukr_uat.moneyveo.services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.NON_FIELD_ERRORS;

/**
 * Created by aleksandr on 4/12/17.
 */

public class ApiUtils {

    public static String parseError(String jsonErrorString) {
        try {

            JSONObject jsonError = new JSONObject(jsonErrorString);
            JSONArray jsonArray = jsonError.getJSONArray(NON_FIELD_ERRORS);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < jsonArray.length(); i++) {
                sb.append((String) jsonArray.get(i));
                sb.append(" ");
            }
            return sb.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
