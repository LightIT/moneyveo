package com.moneyveo.ukr_uat.moneyveo.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aleksandr on 4/12/17.
 */

public class AuthProviderModel {

    @SerializedName("Result")
    private int result;

    @SerializedName("ResultDescription")
    private String resultDescription;

    @SerializedName("Reasons")
    private List<ReasonModel> reasons;

    @SerializedName("ModelErrors")
    private List<ErrorModel> errors;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    public List<ReasonModel> getReasons() {
        return reasons;
    }

    public void setReasons(List<ReasonModel> reasons) {
        this.reasons = reasons;
    }
}
