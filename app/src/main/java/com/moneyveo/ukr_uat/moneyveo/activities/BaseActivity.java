package com.moneyveo.ukr_uat.moneyveo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.moneyveo.ukr_uat.moneyveo.api.ApiEvent;
import com.moneyveo.ukr_uat.moneyveo.services.DataLoaderService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by aleksandr on 4/12/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEventReceived(ApiEvent event){
        onApiEventReceived(event);
    }

    protected void requestDataLoader(String action, Bundle extras) {
        Intent intent = new Intent(this, DataLoaderService.class);
        intent.setAction(action);
        if (extras != null)
            intent.putExtras(extras);
        startService(intent);
    }

    abstract void onApiEventReceived(ApiEvent event);
}
