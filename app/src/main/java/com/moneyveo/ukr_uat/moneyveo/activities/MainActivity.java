package com.moneyveo.ukr_uat.moneyveo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.moneyveo.ukr_uat.moneyveo.Dialog;
import com.moneyveo.ukr_uat.moneyveo.R;
import com.moneyveo.ukr_uat.moneyveo.Utils;
import com.moneyveo.ukr_uat.moneyveo.api.ApiEvent;

import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTH_PROVIDER;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.IS_INTERNET_ACCESS;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PROVIDER_TOKEN;

public class MainActivity extends BaseActivity {

    private Button exitButton;
    private TextView providerToken, token, providerInfo;
    private static final String DIALOG_FRAGMENT_TAG = "DialogFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Utils.getToken(getApplicationContext()).equals("")) {
            openLogin();
            return;
        }
        authProvider();
        exitButton = (Button) findViewById(R.id.email_sign_out_button);
        providerToken = (TextView) findViewById(R.id.provide_token);
        token = (TextView) findViewById(R.id.token);
        providerInfo = (TextView) findViewById(R.id.provider_info);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.removeToken(MainActivity.this);
                openLogin();
                return;
            }
        });
        providerToken.setText("Provide Token: " + Utils.getProviderToken(this));
        token.setText("Token: " + Utils.getToken(this));
    }

    @Override
    void onApiEventReceived(ApiEvent event) {
        if (event.getEvent().equals(AUTH_PROVIDER)) {
            if (event.isSuccess()) {
                providerInfo.setText(getString(R.string.provider_valid));
            } else {
                providerInfo.setText(getString(R.string.provider_invalid));
                    Dialog dialog = new Dialog();
                    Bundle bundle = new Bundle();
                    bundle.putString(Dialog.MESSAGE, event.getErrorMessage());
                    dialog.setArguments(bundle);
                    dialog.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
                    return;
            }
        } else if (event.getEvent().equals(IS_INTERNET_ACCESS)) {
            Dialog dialog = new Dialog();
            Bundle bundle = new Bundle();
            bundle.putString(Dialog.MESSAGE, getString(R.string.no_internet_access));
            dialog.setArguments(bundle);
            dialog.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
            return;
        }
    }

    public void openLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP
                        |Intent.FLAG_ACTIVITY_CLEAR_TASK
                        |Intent.FLAG_ACTIVITY_NEW_TASK
        );
        startActivity(intent);
    }

    void authProvider() {
        Bundle bundle = new Bundle();
        bundle.putString(PROVIDER_TOKEN, Utils.getProviderToken(this));
        requestDataLoader(PROVIDER_TOKEN, bundle);
    }
}
