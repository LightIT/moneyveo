package com.moneyveo.ukr_uat.moneyveo.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aleksandr on 4/12/17.
 */

public class ErrorModel {

    @SerializedName("Field")
    private String field;

    @SerializedName("Errors")
    private List<String> errors;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
