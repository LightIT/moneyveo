package com.moneyveo.ukr_uat.moneyveo.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by aleksandr on 4/12/17.
 */

public class BodyModel {

    private String identifier;

    private String password;

    public BodyModel(String identifier, String password) {
        this.identifier = identifier;
        this.password = password;
    }
}
