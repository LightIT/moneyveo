package com.moneyveo.ukr_uat.moneyveo.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aleksandr on 4/12/17.
 */

public class ReasonModel {

    @SerializedName("Code")
    private float code;

    @SerializedName("CodeDescription")
    private String codeDescription;

    @SerializedName("Details")
    private String details;

    @SerializedName("ModelErrors")
    private List<ErrorModel> errors;

    public float getCode() {
        return code;
    }

    public void setCode(float code) {
        this.code = code;
    }

    public String getCodeDescription() {
        return codeDescription;
    }

    public void setCodeDescription(String codeDescription) {
        this.codeDescription = codeDescription;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<ErrorModel> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorModel> errors) {
        this.errors = errors;
    }
}
