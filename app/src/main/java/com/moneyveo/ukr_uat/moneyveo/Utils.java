package com.moneyveo.ukr_uat.moneyveo;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by aleksandr on 4/12/17.
 */

public class Utils {

    private static final String USER_PREF = "user_pref";
    private static final String TOKEN = "token";
    private static final String PROVIDER_TOKEN = "provider_token";

    public static void saveToken(Context context, String token) {
        SharedPreferences sp = context.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        sp.edit().putString(TOKEN, token).commit();
    }

    public static String getToken(Context context) {
        SharedPreferences sp = context.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        return sp.getString(TOKEN, "");
    }

    public static void setProvideerToken(Context context, String providerToken) {
        SharedPreferences sp = context.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        sp.edit().putString(PROVIDER_TOKEN, providerToken);
    }

    public static String getProviderToken(Context context) {
        SharedPreferences sp = context.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
//        return sp.getString(PROVIDER_TOKEN, "");
        return "5a22bc22-1c0d-429a-b5aa-538d7b7bbb8c";
    }

    public static void removeToken(Context context) {
        SharedPreferences sp = context.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        sp.edit().remove(TOKEN).commit();
    }
}
