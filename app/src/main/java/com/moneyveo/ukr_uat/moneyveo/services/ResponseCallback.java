package com.moneyveo.ukr_uat.moneyveo.services;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aleksandr on 4/12/17.
 */

public abstract class ResponseCallback<T> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {

        if (response.isSuccessful()) {
            onSuccess(call, response);
            return;
        }

        onError(call, response);

    }

    public abstract void onSuccess(Call<T> call, Response<T> response);

    public abstract  void onError(Call<T> call, Response<T> response);

}
