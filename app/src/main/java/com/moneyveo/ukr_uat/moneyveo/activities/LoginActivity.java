package com.moneyveo.ukr_uat.moneyveo.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.moneyveo.ukr_uat.moneyveo.Dialog;
import com.moneyveo.ukr_uat.moneyveo.R;
import com.moneyveo.ukr_uat.moneyveo.api.ApiEvent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static android.Manifest.permission.READ_CONTACTS;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.AUTH;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.EMAIL;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.IS_INTERNET_ACCESS;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PASSWORD;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.PROVIDER_TOKEN;
import static com.moneyveo.ukr_uat.moneyveo.services.ServiceConstants.USER_LOGIN;

/**
 * Created by aleksandr on 4/12/17.
 */

public class LoginActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    private static final String DIALOG_FRAGMENT_TAG = "DialogFragment";


    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private TextInputLayout mTextInputLayoutEmail, mTextInputLayoutPassword;
    private Button mEmailSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mTextInputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        mTextInputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (TextUtils.isEmpty(mEmailView.getText().toString()) & !hasFocus) {
                    mTextInputLayoutEmail.setError(getString(R.string.error_field_required));
                } else if (!isEmailValid(mEmailView.getText().toString()) & !hasFocus) {
                    mTextInputLayoutEmail.setError(getString(R.string.wrong_email));
                } else {
                    mTextInputLayoutEmail.setError(null);
                }
                activeLogInButton();
            }
        });
        mEmailView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                activeLogInButton();
            }
        });
        /**
         * Refactor
         */
        mEmailView.setText("firefox_1d2f478ce45d4ce0b648da39defbfb5b_9@mv.ua");
        mPasswordView.setText("12345aQ");

        populateAutoComplete();


        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_ACTION_GO) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mPasswordView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (TextUtils.isEmpty(mPasswordView.getText().toString()) & !hasFocus) {
                    mTextInputLayoutPassword.setError(getString(R.string.error_field_required));
                } else if (!isPasswordValid(mPasswordView.getText().toString()) & !hasFocus) {
                    mTextInputLayoutPassword.setError(getString(R.string.error_invalid_password));
                } else {
                    mTextInputLayoutPassword.setError(null);
                }
                activeLogInButton();
            }
        });
        mPasswordView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(mPasswordView.getText().toString())) {
                    mTextInputLayoutPassword.setError(getString(R.string.error_field_required));
                } else if (isPasswordValid(mPasswordView.getText().toString())) {
                    mTextInputLayoutPassword.setError(null);
                }
                activeLogInButton();
            }
        });


        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);


        /**
         * Refactor
         */
        addAdapterToView();

    }

    private void activeLogInButton() {
        mEmailSignInButton.setEnabled(isEmailValid(mEmailView.getText().toString())
                & isPasswordValid(mPasswordView.getText().toString())
                ? true : false);
    }

    private void addAdapterToView() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Account[] accounts = AccountManager.get(this).getAccounts();
        Set<String> emailSet = new HashSet<>();
        for (Account account : accounts) {
            if (EMAIL_PATTERN.matcher(account.name).matches()) {
                emailSet.add(account.name);
            }
        }

        addEmailsToAutoComplete(new ArrayList<>(emailSet));
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
                addAdapterToView();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!isPasswordValid(password)) {
            mTextInputLayoutPassword.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (!isEmailValid(email)) {
            mTextInputLayoutEmail.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        } else if (TextUtils.isEmpty(email)) {
            mTextInputLayoutEmail.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            login(email, password);
        }
    }

    void login(String email, String password) {
        Bundle bundle = new Bundle();
        bundle.putString(EMAIL, email);
        bundle.putString(PASSWORD, password);
        requestDataLoader(USER_LOGIN, bundle);
    }

    private boolean isEmailValid(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 3;
    }

    private void showProgress(boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
        mEmailView.setThreshold(1);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    @Override
    void onApiEventReceived(ApiEvent event) {
        if (event.getEvent().equals(IS_INTERNET_ACCESS)) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(false);
                }
            });

            Dialog dialog = new Dialog();
            Bundle bundle = new Bundle();
            bundle.putString(Dialog.MESSAGE, getString(R.string.no_internet_access));
            dialog.setArguments(bundle);
            dialog.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
            return;
        }
        if (!event.getEvent().equals(AUTH))
            return;

        if (event.isSuccess()) {
            onLoginSuccess();
        } else {
            showProgress(false);
            Dialog dialog = new Dialog();
            Bundle bundle = new Bundle();
            bundle.putString(Dialog.MESSAGE, event.getErrorMessage());
            dialog.setArguments(bundle);
            dialog.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
            return;
        }
    }


    void onLoginSuccess() {
        returnToMainActivity();
    }

    void returnToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP
                        |Intent.FLAG_ACTIVITY_CLEAR_TASK
                        |Intent.FLAG_ACTIVITY_NEW_TASK
        );
        startActivity(intent);
    }
}
