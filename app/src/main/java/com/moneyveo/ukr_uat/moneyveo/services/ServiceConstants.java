package com.moneyveo.ukr_uat.moneyveo.services;

/**
 * Created by aleksandr on 4/12/17.
 */

public class ServiceConstants {

    public static final String IS_INTERNET_ACCESS = "is_internet_access";
    public static final String USER_LOGIN = "user_login";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String TOKEN = "Token";
    public static final String PROVIDER_TOKEN = "provider_token";
    public static final String AUTH = "auth";
    public static final String AUTH_PROVIDER = "auth_provider";
    public static final String NON_FIELD_ERRORS = "non_field_errors";

    public static final String SUCCEEDED = "Succeeded";
    public static final String FAILED = "Failed";

    public static final float COMMUNICATION_GENERAL_ERROR = 0.101f;
    public static final float COMMUNICATION_HTTPS_IS_REQUIERED = 0.102f;
    public static final float INVALID_PROVIDER_TOKEN = 1.103f;

    public static final float GENERAL_VALIDATION_PARAMETER_IS_REQUIRED = 3.101f;
    public static final float GENERAL_VALIDATION_AUTH_HEADER_PARSE_ERROR = 3.102f;
    public static final float GENERAL_VALIDATION_WORKFLOW_VALIDATION_ERROR = 3.103f;
    public static final float GENERAL_VALIDATION_MODEL_VALIDATION_ERROR = 3.104f;

    public static final float PROVIDER_AUTHORIZATION_AUTHORIZED = 1.0f;
    public static final float PROVIDER_AUTHORIZATION_TOKEN_NOT_FOUND = 1.101f;
    public static final float PROVIDER_AUTHORIZATION_PROVIDER_IS_BLOCKED = 1.102f;
    public static final float PROVIDER_AUTHORIZATION_PARAMETERS_VALIDATION_IS_FAILED = 1.103f;
    public static final float PROVIDER_AUTHORIZATION_AUTHORIZATION_REQUIERED = 1.104f;

    public static final float USER_DOCUMENT_VALIDATION_DOCUMENT_NOT_FOUND = 7.101f;
    public static final float USER_DOCUMENT_VALIDATION_IMPOSSIBLE_TO_CONFIRM_DOCUMENTS = 7.102f;

    public static final float AUTHORIZATION_AUTHORIZED_BY_EMAIL = 2.0f;
    public static final float AUTHORIZATION_INCORRECT_PASSWORD = 2.101f;
    public static final float AUTHORIZATION_PARAMETERS_VALIDATION_FAILED = 2.102f;
    public static final float AUTHORIZATION_USER_NOT_FOUND = 2.103f;
    public static final float AUTHORIZATION_FOUND_MORE_THAN_ONE_USER_WITH_THE_PHONE_NUMBER = 2.104f;
    public static final float AUTHORIZATION_ACCOUNT_IS_BLOCKED = 2.105f;
    public static final float AUTHORIZATION_TOKEN_NOT_FOUND = 2.106f;
    public static final float AUTHORIZATION_TOKEN_NOT_ACTIVE = 2.107f;
    public static final float AUTHORIZATION_AUTHORIZATION_REQUIRED = 2.108f;

    public static final float CARD_VALIDATION_CARD_NOT_FOUND = 4.101f;
    public static final float CARD_VALIDATION_CARD_WAS_VERIFIED = 4.102f;
    public static final float CARD_VALIDATION_USER_NOT_OWNS_THIS_CARD = 4.103f;
    public static final float CARD_VALIDATION_CARD_IS_INVALID_FOR_PRIMARY = 4.104f;
    public static final float CARD_VALIDATION_INTERNAL_SERVICE_PROBLAM = 4.105f;
    public static final float CARD_VALIDATION_WAS_NOT_CREATED = 4.106f;
    public static final float CARD_VALIDATION_PAYMENT_NOT_FOUND = 4.107f;
    public static final float CARD_VALIDATION_PAYMENT_DECLINED = 4.108f;
    public static final float CARD_VALIDATION_PREAUTH_ATTEMPTS_EXCEEDED = 4.109f;
    public static final float CARD_VALIDATION_PREAUTH_SUM_INVALID = 4.110f;

    public static final float USER_REGISTRATION_USER_ALREADY_EXIST = 5.101f;
    public static final float USER_REGISTRATION_REGISTERED = 5.102f;

    public static final float PROMO_CODE_ACTIVATION_SUCCEEDED = 6.0f;
    public static final float PROMO_CODE_ACTIVATION_FAILED = 6.1f;
    public static final float PROMO_CODE_ACTIVATION_NEED_AUTHORIZE = 6.2f;
    public static final float PROMO_CODE_ACTIVATION_EXPIRED = 6.3f;
}
